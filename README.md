# Backend Django IFML

Repositori kode backend untuk frontend yang dibuat secara otomatis pada https://gitlab.com/RSE-Lab-Fasilkom-UI/prices-2/ifml-pwa-transformation-tool/-/tree/react-graphql.

## Instalasi
**Persyaratan**:
* [Python 3](https://www.python.org/downloads/)
* [Pip 3](https://pip.pypa.io/en/stable/installing/)
* Koneksi interne[t](https://www.youtube.com/watch?v=615m5f3nX5A)

**Menjalankan Backend**:
1. Menginstall semua *package* yang ada di dalam `requirements.txt`. Untuk menginstall Jalankan `pip install -r requirements.txt`. 
2. Sebelum menjalankan aplikasi, pengguna dapat menggunakan perintah `python manage.py migrate` untuk membuat database sqlite dengan nama `db.sqlite3`.
3. Menjalankan aplikasi dengan perintah `python manage.py runserver 1234`