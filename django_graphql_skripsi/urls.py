"""django_graphql_skripsi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.http import HttpResponseNotAllowed, JsonResponse
from django_graphql_skripsi.skripsi.schema import schema
from django.contrib import admin
from django.urls import path
from graphene_django.views import GraphQLView
import json

def graphql_function(request):
    """
    request
        method => POST
        body => {graphql: graphql query string, token: token string}
    return => graphql json that obeys standars
    """
    if request.method == 'POST':
        body = json.loads(request.body)
        graphql_request = body['graphql']
        token = body['token']
        result = schema.execute(graphql_request).to_dict()
        print(graphql_request)
        print(result)
        return JsonResponse(result)
    else:
        return HttpResponseNotAllowed(['POST'])

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/graphiql', GraphQLView.as_view(graphiql=True)),
    path('api/graphql', graphql_function),
]
