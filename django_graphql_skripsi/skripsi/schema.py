from django_graphql_skripsi.skripsi.models import Income, Expense, Program, Summary
from rest_framework.serializers import ModelSerializer
from graphene_django.filter import DjangoFilterConnectionField
from graphene_django.types import DjangoObjectType
from graphene import ObjectType, Field, Argument, Int, Schema, List, InputObjectType, String, Mutation
from graphene.types import datetime


class GeneralModelSerializers(ModelSerializer):
    class Meta:
        model = None
        fields = '__all__'


class SchemaUtility(object):
    @staticmethod
    def models_to_dict(list_of_models):
        custom_serializers = GeneralModelSerializers
        if len(list_of_models) > 0:
            custom_serializers.Meta.model = type(list_of_models[0])
            return [custom_serializers(i).data for i in list_of_models]
        else:
            return []

    @staticmethod
    def one_model_to_dict(model):
        custom_serializers = GeneralModelSerializers
        custom_serializers.Meta.model = type(model)
        return custom_serializers(model).data


class IncomeType(DjangoObjectType):
    class Meta:
        model = Income


class ExpenseType(DjangoObjectType):
    class Meta:
        model = Expense


class ProgramType(DjangoObjectType):
    class Meta:
        model = Program


class SummaryType(ObjectType):
    id = Int()
    datestamp = String()
    description = String()
    income = Int()
    expense = Int()
    programName = String()


class SaveIncome(Mutation):
    class Arguments:
        datestamp = datetime.Date()
        description = String()
        amount = Int()
        idCoa = String()
        programName = String()

    save_income = List(IncomeType)

    def mutate(root, info, datestamp, description, amount, idCoa, programName):
        new_income = Income(datestamp=datestamp, description=description,
                            amount=amount, idCoa=idCoa, programName=programName)
        new_income.save()
        return SaveIncome(save_income=Income.objects.all())


class SaveSummary(Mutation):
    class Arguments:
        datestamp = String()
        description = String()
        income = Int()
        expense = Int()
        programName = String()

    save_summary = List(SummaryType)

    def mutate(root, info, datestamp, description, income, expense, programName):
        income_obj = Income.objects.get(pk=income)
        expense_obj = Expense.objects.get(pk=expense)
        new_summary = Summary(datestamp=datestamp, description=description,
                              income=income_obj, expense=expense_obj, programName=programName)
        new_summary.save()
        model_dict = SchemaUtility.models_to_dict(Summary.objects.all())
        return SaveSummary(save_summary=model_dict)


class SaveExpense(Mutation):
    class Arguments:
        datestamp = datetime.Date()
        description = String()
        amount = Int()
        idCoa = String()
        programName = String()

    save_expense = List(ExpenseType)

    def mutate(root, info, datestamp, description, amount, idCoa, programName):
        new_expense = Expense(datestamp=datestamp, description=description,
                              amount=amount, idCoa=idCoa, programName=programName)
        new_expense.save()
        return SaveExpense(save_expense=Expense.objects.all())


class SaveProgram(Mutation):
    class Arguments:
        name = String()
        description = String()
        target = String()
        partner = String()
        logo_url = String()

    save_program = List(ProgramType)

    def mutate(root, info, name, description, target, partner, logo_url):
        new_program = Program(name=name, description=description,
                              target=target, partner=partner, logo_url=logo_url)
        new_program.save()
        return SaveProgram(save_program=Program.objects.all())


class UpdateProgram(Mutation):
    class Arguments:
        id = Int()
        name = String(required=False)
        description = String(required=False)
        target = String(required=False)
        partner = String(required=False)
        logo_url = String(required=False)

    update_program = List(ProgramType)

    def mutate(root, info, id, **kwargs):
        current_program = Program.objects.get(id=id)
        current_program.name = kwargs.get("name", current_program.name)
        current_program.description = kwargs.get(
            "description", current_program.description)
        current_program.target = kwargs.get("target", current_program.target)
        current_program.partner = kwargs.get(
            "partner", current_program.partner)
        current_program.logo_url = kwargs.get(
            "logo_url", current_program.logo_url)
        current_program.save()

        return UpdateProgram(update_program=Program.objects.all())


class DeleteIncome(Mutation):
    class Arguments:
        id = Int()

    delete_income = List(IncomeType)

    def mutate(root, info, id):
        Income.objects.get(pk=id).delete()
        return DeleteIncome(delete_income=Income.objects.all())


class DeleteExpense(Mutation):
    class Arguments:
        id = Int()

    delete_expense = List(ExpenseType)

    def mutate(root, info, id):
        Expense.objects.get(pk=id).delete()
        return DeleteExpense(delete_expense=Expense.objects.all())


class DeleteProgram(Mutation):
    class Arguments:
        id = Int()

    delete_program = List(ProgramType)

    def mutate(root, info, id):
        Program.objects.get(pk=id).delete()
        return DeleteProgram(delete_program=Program.objects.all())


class DeleteSummary(Mutation):
    class Arguments:
        id = Int()

    delete_summary = List(SummaryType)

    def mutate(root, info, id):
        Summary.objects.get(pk=id).delete()

        return DeleteSummary(delete_summary=SchemaUtility.models_to_dict(Summary.objects.all()))


class AllQuery(ObjectType):
    detail_income = Field(IncomeType, id=Argument(Int, required=True))
    list_income = List(IncomeType)
    detail_expense = Field(ExpenseType, id=Argument(Int, required=True))
    list_expense = List(ExpenseType)
    detail_program = Field(ProgramType, id=Argument(Int, required=True))
    list_program = List(ProgramType)
    detail_summary = Field(SummaryType, id=Argument(Int, required=True))
    list_summary = List(SummaryType)

    @staticmethod
    def resolve_detail_income(parent, info, id):
        return Income.objects.get(pk=id)

    @staticmethod
    def resolve_list_income(parent, info):
        return Income.objects.all()

    @staticmethod
    def resolve_detail_expense(parent, info, id):
        return Expense.objects.get(pk=id)

    @staticmethod
    def resolve_list_expense(parent, info):
        return Expense.objects.all()

    @staticmethod
    def resolve_detail_program(parent, info, id):
        return Program.objects.get(pk=id)

    @staticmethod
    def resolve_list_program(parent, info):
        return Program.objects.all()

    @staticmethod
    def resolve_detail_summary(parent, info, id):
        return SchemaUtility.one_model_to_dict(Summary.objects.get(pk=id))

    @staticmethod
    def resolve_list_summary(parent, info):
        return SchemaUtility.models_to_dict(Summary.objects.all())


class AllMutation(ObjectType):
    save_income = SaveIncome.Field()
    delete_income = DeleteIncome.Field()
    save_expense = SaveExpense.Field()
    delete_expense = DeleteExpense.Field()
    save_program = SaveProgram.Field()
    delete_program = DeleteProgram.Field()
    update_program = UpdateProgram.Field()
    save_summary = SaveSummary.Field()
    delete_summary = DeleteSummary.Field()


schema = Schema(query=AllQuery, mutation=AllMutation)
