from django.db import models

# Create your models here.


class Income(models.Model):
    datestamp = models.DateField()
    description = models.CharField(max_length=150)
    amount = models.IntegerField()
    idCoa = models.CharField(max_length=150)
    programName = models.CharField(max_length=150)


class Expense(models.Model):
    datestamp = models.DateField()
    description = models.CharField(max_length=150)
    amount = models.IntegerField()
    idCoa = models.CharField(max_length=150)
    programName = models.CharField(max_length=150)


class Program(models.Model):
    name = models.CharField(max_length=150)
    description = models.CharField(max_length=150)
    target = models.CharField(max_length=150)
    partner = models.CharField(max_length=150)
    logo_url = models.CharField(max_length=150)


class Summary(models.Model):
    datestamp = models.DateField()
    description = models.CharField(max_length=150)
    income = models.ForeignKey(Income, on_delete=models.CASCADE)
    expense = models.ForeignKey(Expense, on_delete=models.CASCADE)
    programName = models.CharField(max_length=150)
